from flask import Flask
from flask import render_template, redirect, request
import os

MAX_LENGTH = 5000

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/predict/', methods=['POST'])
def predict():
    seq = request.form.get('seq','')
    if seq == '':
        return render_template('index.html',
                               error="Empty input")

    seq = ''.join([s.strip().upper() for s in seq.split()])[:MAX_LENGTH]
    result = os.popen('python classify.py ' + seq).read().strip()
    return render_template('index.html',
                           input_seq=seq,
                           result_seq=result,
                           zip_seq=zip(seq,result))
    
