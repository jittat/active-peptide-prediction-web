import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import sys

ALPHA = "ARNDCEQGHILKMFPSTWYVXBZ"
seq_idx = {a: idx for a,idx in zip(ALPHA,range(len(ALPHA)))}

def prepare_input(seq):
    new_seq = []
    for a in seq:
        if a not in ALPHA:
            new_seq.append('X')
        else:
            new_seq.append(a)
    
    return torch.tensor([seq_idx[x] for x in new_seq], dtype=torch.long)

def prepare_tag(tag):
    return torch.tensor([int(x) for x in tag], dtype=torch.long)

class LSTMTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)

        # The LSTM takes word embeddings as inputs, and outputs hidden states
        # with dimensionality hidden_dim.
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, bidirectional=True)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(hidden_dim * 2, tagset_size)

    def forward(self, sentence):
        embeds = self.word_embeddings(sentence)
        lstm_out, _ = self.lstm(embeds.view(len(sentence), 1, -1))
        tag_space = self.hidden2tag(lstm_out.view(len(sentence), -1))
        tag_scores = F.log_softmax(tag_space, dim=1)
        return tag_scores

def classify(model, sentence):
    sentence_in = prepare_input(sentence)
    tag_scores = model(sentence_in)
    prediction = [int(torch.argmax(s)) for s in tag_scores]
    return ''.join([str(x) for x in prediction])

def main():
    input_sequence = sys.argv[1]
    
    torch.manual_seed(1)

    EMBEDDING_DIM = 20
    HIDDEN_DIM = 30

    model = LSTMTagger(EMBEDDING_DIM, HIDDEN_DIM, len(ALPHA), 2)
    model.load_state_dict(torch.load('models/model-100epoch-1.pt'))
    model.eval()

    print(classify(model, input_sequence))


if __name__ == '__main__':
    main()
